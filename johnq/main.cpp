#include <iostream>
#include <string>
#include <iomanip>
#include <ctime>
#include <cstdlib>
#include <algorithm>
#include <locale>
#include "calendarhelper.hxx"

using namespace std;


int main(int argc, char** argv) {

	tm* thetime;
	time_t t = time(nullptr);
	string tz = "TZ=Europe/Stockholm";
	// Set the environment time zone
	putenv((char*)tz.data());
	thetime = localtime(&t);                        // Initialize tm struct, with localtime data

	int numdays;
	int month;
	ArgsCalendar cmdarg{0, 0, 0, "none"};

	// If there's no arguments in the command then it will print the current month.
	// TODO: implement exceptions for arguments
	if (argc > 1){
		if ( !par_reader(argc, argv, cmdarg) ){
			cout << "Wrong argument exit" << '\n';
			return 128;
		}
	}
	if (cmdarg.cmd == "curmonth")
		(*thetime).tm_mon = cmdarg.monthInitial;
	if (cmdarg.cmd == "monthyear") {
		(*thetime).tm_mon = cmdarg.monthInitial;
		(*thetime).tm_year = cmdarg.year;
	}
	// It is a leap year. February.
	bool isleap = is_leap((*thetime).tm_year);

	// Determine month number of days, zero-based values.
	numdays = numdaysmonth((*thetime).tm_mon, isleap);

	// Which is the first weekday for selected month.
	monthFirstDay(thetime);
//	cout << "First day is: " << (*thetime).tm_wday << '\n';

	printCalendar(thetime, numdays);
	return 0;
}

